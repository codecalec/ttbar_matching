import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from pathlib import Path
from typing import List


os.environ["KERAS_BACKEND"] = "theano"
import keras  # noqa: E402

MC_DATA_FOLDER = Path("/atlas/aveltman/DATAMC/TopMass/topParentNtuples/")

PROJECT_FOLDER = Path("/atlas/aveltman/LeptonMatchingModel")
PLOT_FOLDER = PROJECT_FOLDER / "plots"
DATA_FOLDER = PROJECT_FOLDER / "data"

if not os.path.exists(PLOT_FOLDER):
    os.makedirs(PLOT_FOLDER)

if not os.path.exists(DATA_FOLDER):
    os.makedirs(DATA_FOLDER)


def paramater_tuning(filenames: List[str], chainname: str):
    from dataframes import generateDataFrames
    from models import createElectronMatchModel, createMuonMatchModel
    from functools import partial

    electron_dataframe = pd.Dataframe()
    muon_dataframe = pd.Dataframe()
    for filename in filenames:
        e_df, m_df = generateDataFrames(
            filename, chainname
        )
        electron_dataframe.append(e_df)
        electron_dataframe.append(m_df)
        print(f"Added {filename}")

    X_train_e, X_test_e, y_train_e, y_test_e = preprocess(electron_dataframe)
    X_train_m, X_test_m, y_train_m, y_test_m = preprocess(muon_dataframe)

    activations = [
        partial(keras.layers.Activation, "relu"),
        keras.layers.LeakyReLU
    ]
    optimizers = [
        keras.optimizers.SGD(
            lr=0.01, momentum=0.9, decay=1e-4, nesterov=True
        ),
        keras.optimizers.Adam(),
    ]
    num_of_nodes = [16, 32, 64, 128]
    batch_size = [16, 32, 64]
    params = dict(
        activation=activations,
        optimizer=optimizers,
        num_of_nodes=num_of_nodes,
        batch_size=batch_size
    )

    from keras.wrappers.scikit_learn import KerasClassifier
    from sklearn.model_selection import RandomizedSearchCV
    electron_model = KerasClassifier(
        build_fn=createElectronMatchModel,
        epochs=50,
        batch_size=16
    )

    muon_model = KerasClassifier(
        build_fn=createMuonMatchModel,
        epochs=50,
        batch_size=16
    )

    np.random.seed(42)

    rscv_electron = RandomizedSearchCV(
        electron_model,
        param_distributions=params,
        cv=3,
        n_iter=10
    )

    rscv_muon = RandomizedSearchCV(
        muon_model,
        param_distributions=params,
        cv=3,
        n_iter=10
    )

    rscv_electron_results = rscv_electron.fit(X_train_e, y_train_e)
    print('Best electron score is: {} using {}'.format(
        rscv_electron_results.best_score_, rscv_electron_results.best_params_)
    )

    rscv_muon_results = rscv_muon.fit(X_train_m, y_train_m)
    print('Best muon score is: {} using {}'.format(
        rscv_muon_results.best_score_, rscv_muon_results.best_params_)
    )


def run_model(filenames: List[str], chainname: str):
    from dataframes import generateDataFrames

    electron_dataframe, muon_dataframe = generateDataFrames(
        filenames[0], chainname
    )
    print(f"Added {filenames[0]}")
    for filename in filenames[1:]:
        el_df, mu_df = generateDataFrames(filename, chainname)
        electron_dataframe = electron_dataframe.append(el_df)
        muon_dataframe = muon_dataframe.append(mu_df)
        print(f"Added {filename}")

    X_train_e, X_test_e, y_train_e, y_test_e = preprocess(electron_dataframe)
    X_train_m, X_test_m, y_train_m, y_test_m = preprocess(muon_dataframe)

    from models import createElectronMatchModel, createMuonMatchModel

    activation = keras.layers.LeakyReLU
    optimizer = keras.optimizers.Adam()
    num_of_nodes = 64

    electron_model = createElectronMatchModel(
        activation, optimizer, num_of_nodes
    )
    muon_model = createMuonMatchModel(
        activation, optimizer, num_of_nodes
    )

    electron_model = train_model(
        electron_model, "Electron Model", X_train_e, y_train_e
    )
    muon_model = train_model(
            muon_model, "Muon Model", X_train_m, y_train_m
    )

    evaluate_model(electron_model, "Electron Model", X_test_e, y_test_e)
    evaluate_model(muon_model, "Muon Model", X_test_m, y_test_m)


def preprocess(
    dataframe: pd.DataFrame,
) -> (np.ndarray, np.ndarray, np.ndarray, np.ndarray):
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.model_selection import train_test_split

    dataframe = dataframe.drop(
        dataframe.columns[dataframe.columns.str.contains(r"[A-z]on\_phi$")],
        axis=1,
    )

    y = dataframe.pop("isMatch").to_numpy()

    scaler = MinMaxScaler()
    X = scaler.fit_transform(dataframe)

    feature_selection(X, y)

    X_train, X_test, y_train, y_test = train_test_split(X, y)
    return X_train, X_test, y_train, y_test


def train_model(
    model: keras.Model, name: str,
    X_train: np.ndarray, y_train: np.ndarray,
    verbose: int = 2,
):
    print("Training mode:", name)
    checkpoint_cb = keras.callbacks.ModelCheckpoint(
        name + ".h5", save_best_only=True
    )
    early_stopping_cb = keras.callbacks.EarlyStopping(
        patience=30, restore_best_weights=True
    )
    callbacks = [checkpoint_cb, early_stopping_cb]

    history = model.fit(
        X_train,
        y_train,
        epochs=500,
        validation_split=0.3,
        batch_size=64,
        callbacks=callbacks,
        verbose=verbose,
    )

    plt.plot(history.history['acc'], label="Training Accuracy")
    plt.plot(history.history['val_acc'], label="Validation Accuracy")
    plt.title(f'{name} Accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend()
    plt.xlim(left=0)
    plt.savefig(PLOT_FOLDER / f"{name}_acc.png", dpi=400)
    plt.close()

    plt.plot(history.history['loss'], label="Training Loss")
    plt.plot(history.history['val_loss'], label="Validation Loss")
    plt.title(f'{name} Loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend()
    plt.xlim(left=0)
    plt.savefig(PLOT_FOLDER / f"{name}_loss.png", dpi=400)
    plt.close()

    model = keras.models.load_model(name + ".h5")
    return model


def evaluate_model(
    model: keras.Model, name: str, X_test: np.ndarray, y_test: np.ndarray
):
    print("Evaluating model:", name)
    results = model.evaluate(X_test, y_test)
    print("\nLoss, accuracy on test data: ")
    print("%0.4f %0.2f%%" % (results[0], results[1] * 100))

    y_pred_classes = model.predict_classes(X_test, batch_size=1)
    plot_confusion_matrix(y_test, y_pred_classes, name)
    y_pred = model.predict(X_test).ravel()
    plot_roc_cruve(y_test, y_pred, name)

    predictions = model.predict(X_test)
    predictions = [int(round(prediction[0])) for prediction in predictions]

    total_count = 0
    correct_count = 0
    for label, prediction in zip(y_test, predictions):
        if (label == 1) and (prediction == 1):
            correct_count += 1
            total_count += 1
        elif (label == 1) and (prediction == 0):
            total_count += 1
    ratio = correct_count/total_count
    print(f"Correct:{correct_count}\nTotal:{total_count}\nRatio:{ratio:.2f}")


def plot_roc_cruve(y_test, y_pred, name):
    import sklearn.metrics as metrics

    fpr, tpr, threshold = metrics.roc_curve(y_test, y_pred)
    roc_auc = metrics.auc(fpr, tpr)

    plt.title(f'{name} Receiver Operating Characteristic')
    plt.plot(fpr, tpr, 'b', label=f'AUC = {roc_auc:.2f}')
    plt.legend(loc='lower right')
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.savefig(PLOT_FOLDER / f"{name}_roc.png", dpi=400)
    plt.close()


def plot_confusion_matrix(y_test: np.ndarray, y_pred: np.ndarray, name: str):
    from sklearn.metrics import confusion_matrix
    from itertools import product

    cm = confusion_matrix(y_test, y_pred, labels=[1, 0])
    plt.matshow(
        cm,
        cmap=plt.cm.Blues
    )
    plt.title(f"{name} Confusion Matrix")
    plt.xticks([0, 1], ["Same", "Different"])
    plt.yticks(
        [0, 1],
        ["Same", "Different"],
    )

    thresh = cm.max() / 2
    for i, j in product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(
            j, i, cm[i, j],
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black",
        )
    plt.savefig(PLOT_FOLDER / f"{name}_cm.png", dpi=400)
    plt.close()


def feature_selection(X, y):
    from sklearn.feature_selection import f_classif, SelectKBest

    fs = SelectKBest(score_func=f_classif, k=5)
    X_selected = fs.fit_transform(X, y)
    print(fs.scores_)
    print(fs.pvalues_)
    return X_selected


if __name__ == "__main__":
    # plt.style.use('atlas')
    files = [
        "mtop169",
        "mtop171",
        "mtop172",
        "mtop172p25",
        "mtop172p5",
        "mtop172p75",
        "mtop173",
        "mtop174",
        "mtop176",
    ]
    filenames = [os.path.join(MC_DATA_FOLDER, f"{f}/{f}") for f in files]
    chainname = "nominal"
    # paramater_tuning(filename, chainname)
    run_model(filenames, chainname)
