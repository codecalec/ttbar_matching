import ROOT
import pandas as pd
import os


def getTChain(filename: str, chainname: str) -> ROOT.TChain:
    chain = ROOT.TChain(chainname)
    chain.Add(filename)
    return chain


def m3l(event):
    Ji = event.Jpsi_selected
    Jpt, Jeta, Jphi, Jm = (
        event.Jpsi_pt[Ji],
        event.Jpsi_eta[Ji],
        event.Jpsi_phi[Ji],
        event.Jpsi_m[Ji],
    )
    J4V = ROOT.TLorentzVector()
    J4V.SetPtEtaPhiM(Jpt, Jeta, Jphi, Jm)

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        Lpt, Leta, Lphi, Le = (
            event.electron_pt[Li],
            event.electron_eta[Li],
            event.electron_phi[Li],
            event.electron_E[Li],
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        Lpt, Leta, Lphi, Le = (
            event.muon_pt[Li],
            event.muon_eta[Li],
            event.muon_phi[Li],
            event.muon_E[Li],
        )
    else:
        print("bad!")
    L4V = ROOT.TLorentzVector()
    L4V.SetPtEtaPhiE(Lpt, Leta, Lphi, Le)
    return (J4V + L4V).M() * 0.001


def deltaR(phi: float, eta: float) -> float:
    from math import sqrt

    return sqrt(phi * phi + eta * eta)


def generateDataFramesFromChain(chain: ROOT.TChain):

    # Electron Features
    Jpsi_features_electron = {
        key: [] for key in ["Jpsi_pt", "Jpsi_phi", "Jpsi_eta"]
    }
    electron_features = {
        key: [] for key in ["electron_pt", "electron_phi", "electron_eta"]
    }
    d_features_electron = {
        key: [] for key in ["isMatch", "met_met", "met_phi", "num_of_jets"]
    }

    # Muon Features
    Jpsi_features_muon = {
        key: [] for key in ["Jpsi_pt", "Jpsi_phi", "Jpsi_eta"]
    }
    muon_features = {key: [] for key in ["muon_pt", "muon_phi", "muon_eta"]}
    d_features_muon = {
        key: [] for key in ["isMatch", "met_met", "met_phi", "num_of_jets"]
    }

    num = 1
    total = chain.GetEntries()

    for event in chain:
        if 0 < m3l(event) < 300:
            if num % 1000 == 0:
                print(f"Event {num} of {total} is being analysed")
            num += 1

            lepton_parent = -2
            if event.electron_selected >= 0:
                Li = event.electron_selected
                if event.electron_truth_pdgId[Li] < 0:
                    lepton_parent = 1
                else:
                    lepton_parent = -1
            elif event.muon_selected >= 0:
                Li = event.muon_selected
                if event.muon_truth_pdgId[Li] < 0:
                    lepton_parent = 1
                else:
                    lepton_parent = -1
            else:
                print("bad!")

            Ji = event.Jpsi_selected
            Jpsi_barcode = event.Jpsi_truthMatch_barcode[Ji]
            Jpsi_parent = -2
            for i, barcode in enumerate(event.truth_barcode):
                if barcode == Jpsi_barcode:
                    Jpsi_topparent = bool(event.truth_topParent[i])
                    Jpsi_antitopparent = bool(event.truth_antiTopParent[i])
                    if (Jpsi_topparent == Jpsi_antitopparent):
                        Jpsi_parent = -2
                    elif (Jpsi_topparent == 1):
                        Jpsi_parent = 1
                    elif (Jpsi_antitopparent == 1):
                        Jpsi_parent = -1
                    break

            isMatch = 1 * (Jpsi_parent == lepton_parent)

            if event.electron_selected >= 0:
                for feature_name, values in electron_features.items():
                    value = getattr(chain, feature_name)
                    values.append(value[Li])

                for feature_name, values in Jpsi_features_electron.items():
                    value = getattr(chain, feature_name)
                    values.append(value[Ji])

                d_features_electron["isMatch"].append(isMatch)
                d_features_electron["met_met"].append(event.met_met)
                d_features_electron["met_phi"].append(event.met_phi)
                d_features_electron["num_of_jets"].append(len(event.m_jet_pt))

            elif event.muon_selected >= 0:
                for feature_name, values in muon_features.items():
                    value = getattr(chain, feature_name)
                    values.append(value[Li])

                for feature_name, values in Jpsi_features_muon.items():
                    value = getattr(chain, feature_name)
                    values.append(value[Ji])

                d_features_muon["isMatch"].append(isMatch)
                d_features_muon["met_met"].append(event.met_met)
                d_features_muon["met_phi"].append(event.met_phi)
                d_features_muon["num_of_jets"].append(len(event.m_jet_pt))

    # Delta features for electron channel
    d_features_electron["d_phi"] = list(
        map(
            float.__sub__,
            Jpsi_features_electron["Jpsi_phi"],
            electron_features["electron_phi"],
        )
    )
    d_features_electron["d_eta"] = list(
        map(
            float.__sub__,
            Jpsi_features_electron["Jpsi_eta"],
            electron_features["electron_eta"],
        )
    )
    d_features_electron["d_R"] = list(
        map(
            deltaR, d_features_electron["d_phi"], d_features_electron["d_eta"]
        )
    )
    d_features_electron["d_phi_met"] = list(
        map(
            float.__sub__,
            d_features_electron["met_phi"],
            electron_features["electron_phi"],
        )
    )

    # Delta features for muon channel
    d_features_muon["d_phi"] = list(
        map(
            float.__sub__,
            Jpsi_features_muon["Jpsi_phi"],
            muon_features["muon_phi"],
        )
    )
    d_features_muon["d_eta"] = list(
        map(
            float.__sub__,
            Jpsi_features_muon["Jpsi_eta"],
            muon_features["muon_eta"],
        )
    )
    d_features_muon["d_R"] = list(
        map(deltaR, d_features_muon["d_phi"], d_features_muon["d_eta"])
    )
    d_features_muon["d_phi_met"] = list(
        map(
            float.__sub__,
            d_features_muon["met_phi"],
            muon_features["muon_phi"],
        )
    )

    electron_channel_features = {
        **electron_features,
        **Jpsi_features_electron,
        **d_features_electron,
    }
    muon_channel_features = {
        **muon_features,
        **Jpsi_features_muon,
        **d_features_muon,
    }

    return (
        pd.DataFrame.from_dict(electron_channel_features),
        pd.DataFrame.from_dict(muon_channel_features),
    )


def generateDataFramesFromROOT(filename: str, chainname: str):
    filename = filename + ".root"
    chain = getTChain(filename, chainname)
    print(chain)
    return generateDataFramesFromChain(chain)


def generateDataFrames(
    filename: str, chainname: str
) -> (pd.DataFrame, pd.DataFrame):
    from matching_model import DATA_FOLDER
    basename = os.path.basename(filename)
    electron_filename, muon_filename = (
        os.path.join(DATA_FOLDER, basename + "_electron.csv"),
        os.path.join(DATA_FOLDER, basename + "_muon.csv"),
    )
    if os.path.exists(electron_filename) and os.path.exists(muon_filename):
        e_df, m_df = (
            pd.read_csv(electron_filename),
            pd.read_csv(muon_filename),
        )
    else:
        e_df, m_df = generateDataFramesFromROOT(filename, chainname)
        e_df.to_csv(electron_filename, index=False)
        m_df.to_csv(muon_filename, index=False)
    print("electron dataframe:", len(e_df.index))
    print("muon dataframe:", len(m_df.index))
    return e_df, m_df


if __name__ == "__main__":
    generateDataFrames(
        "/atlas/aveltman/DATAMC/TopMass/topParentNtuples/"
        + "mtop172p5/mtop172p5.root",
        "nominal",
    )
