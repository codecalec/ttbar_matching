import os
os.environ["KERAS_BACKEND"] = "theano"
import keras  # noqa: E402


def createElectronMatchModel(
    activation,
    optimizer: keras.optimizers,
    num_of_nodes: int
):

    initializer = keras.initializers.he_normal()
    linear_activation = keras.activations.linear

    model = keras.models.Sequential()
    model.add(keras.layers.Dense(
                num_of_nodes,
                input_shape=(12,),
                activation=linear_activation,
                kernel_initializer=initializer,
            ))
    model.add(activation())
    model.add(keras.layers.Dense(
                num_of_nodes,
                activation=linear_activation,
                kernel_initializer=initializer,
            ))
    model.add(activation())
    model.add(keras.layers.Dense(
                num_of_nodes,
                activation=linear_activation,
                kernel_initializer=initializer,
            ))
    model.add(activation())
    model.add(keras.layers.Dense(
                num_of_nodes,
                activation=linear_activation,
                kernel_initializer=initializer,
            ))
    model.add(activation())
    model.add(keras.layers.Dense(
                num_of_nodes,
                activation=linear_activation,
                kernel_initializer=initializer,
            ))
    model.add(keras.layers.Dense(1, activation="sigmoid"))

    model.compile(
        loss="binary_crossentropy",
        optimizer=optimizer,
        metrics=["accuracy"]
    )
    print(model.summary())
    return model


def createMuonMatchModel(
        activation: keras.layers,
        optimizer: keras.optimizers,
        num_of_nodes: int):
    return createElectronMatchModel(activation, optimizer, num_of_nodes)
